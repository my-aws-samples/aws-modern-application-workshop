# -*- encoding: utf-8 -*-
import boto3
from botocore.exceptions import ClientError
import os
import time

AWS_REGION = "us-east-1"
YAML_FILE_PATH = 'vpc.yaml'
params = [
          #{"StackName": "MythicalMysfitsNetwork", "YAML_FILE_PATH": 'core.cfn.yaml'},
          #{"StackName": "MythicalMysfitsCloud9", "YAML_FILE_PATH": 'cloud9.cfn.yaml'},
          #{"StackName": "MythicalMysfitsS3", "YAML_FILE_PATH": 'website.cfn.yaml'},
          {"StackName": "MythicalMysfitsECS", "YAML_FILE_PATH": 'ecs.cfn.yaml'},
         ]

def readfile(path, name):
  with open(os.path.join(os.path.dirname(__file__), name), 'r') as f:
    data=f.read()
  return data

class cloudformation:
  def __init__(self, param):
    self.iam = boto3.client("iam", region_name=AWS_REGION)
    self.cfn = boto3.client("cloudformation", region_name=AWS_REGION)
    self.yaml_file_path = param["YAML_FILE_PATH"]
    del param["YAML_FILE_PATH"]
    param["Capabilities"] = ["CAPABILITY_NAMED_IAM"]
    self.param = param

  def validate_template(self):
    yaml_body = readfile("", self.yaml_file_path)
    self.param["TemplateBody"] = yaml_body
    try:
      self.cfn.validate_template(TemplateBody=self.param["TemplateBody"])
    except ClientError as e:
      print("Err: CFn Validate: %s" % e)

  def create_stack(self):
    kwargs = self.param
    self.cfn.create_stack(**kwargs)

  def describe_stack(self):
    stacks = self.cfn.list_stacks()['StackSummaries']
    stack = [x for x in stacks if self.param['StackName'] == x['StackName']]
    if len(stack) == 0:
      return False
    elif stack[0]['StackStatus'] == 'DELETE_COMPLETE':
      return False
    else:
      return True

  def create_change_set(self):
    self.param['ChangeSetName'] = 'ChangeSet'
    kwargs = self.param
    self.cfn.create_change_set(**kwargs)

  def show_chageset_status(self):
    time.sleep(2)
    for i in range(0,600):
      response = self.cfn.describe_change_set(ChangeSetName=self.param['ChangeSetName'],StackName=self.param['StackName'])
      print('Info: ChangeSetStatus '+response['Status'])
      if 'FAILED' == response['Status']:
        self.cfn.delete_change_set(ChangeSetName=self.param['ChangeSetName'],StackName=self.param['StackName'])
      if not 'PROGRESS' in response['Status']:
        return response
      time.sleep(5)

  def execute_change_set(self):
    self.cfn.execute_change_set(ChangeSetName=self.param['ChangeSetName'],StackName=self.param['StackName'])

  def show_stack_status(self):
    time.sleep(2)
    for i in range(0,1000):
      response = self.cfn.describe_stacks(StackName=self.param['StackName'])['Stacks'][0]
      stack_event = self.cfn.describe_stack_events(StackName=self.param['StackName'])['StackEvents'][0]
      message = stack_event['LogicalResourceId']+' '+stack_event['PhysicalResourceId']+' '+stack_event['ResourceStatus']
      if 'ResourceStatusReason' in stack_event:
        message = message+' '+stack_event['ResourceStatusReason']
      print('Info: StackEventStatus '+message)
      if not 'PROGRESS' in response['StackStatus']:
        return response['StackStatus']
      time.sleep(5)

def main():
  for param in params:
    cfn = cloudformation(param)
    cfn.validate_template()
    if not cfn.describe_stack():
      cfn.create_stack()
    else:
      cfn.create_change_set()
      chageset_status = cfn.show_chageset_status()
      if chageset_status['Status'] == 'CREATE_COMPLETE':
        cfn.execute_change_set()
        cfn.show_stack_status()

if __name__ == '__main__':
  main()